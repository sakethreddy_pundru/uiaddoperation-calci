//
//  ViewController.m
//  UIAddOperation
//
//  Created by admin on 08/12/15.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
-(IBAction)add:(UIButton *)sender; {
    int answer=[value1.text intValue]+[value2.text intValue];
    result.text=[NSString stringWithFormat:@"%d",answer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *bgColor = [[UILabel alloc]init];
    bgColor.backgroundColor=[UIColor colorWithRed:213/255.0f green:219/255.0f blue:218/255.0f alpha:1.0];
    bgColor.frame=self.view.frame;
    [self.view addSubview:bgColor];
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(200, 350, 200, 200)];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(add:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle : @"Add" forState:UIControlStateNormal];
    button.layer.cornerRadius = 5;
    button.backgroundColor = [UIColor colorWithRed:33/255.0f green:112/255.0f blue:141/255.0f alpha:1.0f];
    button.frame=CGRectMake(150, 250, 100, 50);
    [self.view addSubview:button];

    value1 = [[UITextField alloc]init];
    value1.textColor = [UIColor blackColor];
    value1.backgroundColor= [UIColor lightTextColor];
    value1.frame=CGRectMake(50, 120, 75, 50);
    value1.borderStyle=UITextBorderStyleRoundedRect;
    value1.placeholder= @"Value1";
    [self.view addSubview:value1];
    
    value2 = [[UITextField alloc]init];
    value2.textColor = [UIColor blackColor];
    value2.backgroundColor= [UIColor lightTextColor];
    value2.frame=CGRectMake(150, 120, 75, 50);
    value2.borderStyle=UITextBorderStyleRoundedRect;
    value2.placeholder= @"Value2";
    [self.view addSubview:value2];
    
    
    result = [[UITextField alloc]init];
    result.textColor = [UIColor blackColor];
    result.backgroundColor= [UIColor lightTextColor];
    result.frame=CGRectMake(250, 120, 75, 50);
    result.borderStyle=UITextBorderStyleRoundedRect;
    result.placeholder= @"Result";
    [self.view addSubview:result];
    
    

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
